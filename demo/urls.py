"""demo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from demo_site.views import CustomerList, CustomerNew, CustomerEdit, CustomerDelete, \
    InvoiceList, InvoiceNew, InvoiceEdit, InvoiceDelete, CustomerAutocomplete

urlpatterns = [
    url(r'^customers/$', login_required(CustomerList.as_view()), name='customers'),
    url(r'^customer/new/$', login_required(CustomerNew.as_view()), name='customer_new'),
    url(r'^customer/(?P<pk>[0-9]+)/$', login_required(CustomerEdit.as_view()), name='customer_edit'),
    url(r'^customer/(?P<pk>[0-9]+)/delete/$', login_required(CustomerDelete.as_view()), name='customer_delete'),
    url(r'^customer_autocomplete/$', login_required(CustomerAutocomplete.as_view()), name='customer_autocomplete'),
    url(r'^invoices/$', login_required(InvoiceList.as_view()), name='invoices'),
    url(r'^invoice/new/$', login_required(InvoiceNew.as_view()), name='invoice_new'),
    url(r'^invoice/(?P<pk>[0-9]+)/$', login_required(InvoiceEdit.as_view()), name='invoice_edit'),
    url(r'^invoice/(?P<pk>[0-9]+)/delete/$', login_required(InvoiceDelete.as_view()), name='invoice_delete'),
    url(r'^', include('cosmicdb.urls')),
]
