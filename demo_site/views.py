from django.urls import reverse
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django_tables2 import SingleTableView
from dal import autocomplete
from extra_views import CreateWithInlinesView, UpdateWithInlinesView, InlineFormSet

from cosmicdb.views import CosmicSaveErrorDialogsMixin
from cosmicdb.forms import CosmicFormsetHelper
from demo_site.models import Customer, Invoice, InvoiceItem
from demo_site.tables import CustomerTable, InvoiceTable
from demo_site.forms import CustomerForm, CustomerFormWithDelete, InvoiceForm, InvoiceItemForm


class CustomerList(SingleTableView):
    template_name = 'demo_site/customer_list.html'
    model = Customer
    table_class = CustomerTable
    table_pagination = {
        'per_page': 10
    }


class CustomerNew(CosmicSaveErrorDialogsMixin, CreateView):
    template_name = 'demo_site/customer_new.html'
    model = Customer
    form_class = CustomerForm


class CustomerEdit(CosmicSaveErrorDialogsMixin, UpdateView):
    template_name = 'demo_site/customer_edit.html'
    model = Customer
    form_class = CustomerFormWithDelete


class CustomerDelete(DeleteView):
    template_name = 'demo_site/customer_delete.html'
    model = Customer

    def get_success_url(self):
        return reverse('customers')


class CustomerAutocomplete(autocomplete.Select2QuerySetView):
    def get_result_label(self, result):
        return '%s - %s' % (result.code, result.name)

    def get_queryset(self):
        qs = Customer.objects.all()
        if self.q:
            qs = qs.filter(code__icontains=self.q, name__icontains=self.q)
        return qs.order_by('code', 'name')


class InvoiceList(SingleTableView):
    template_name = 'demo_site/invoice_list.html'
    model = Invoice
    table_class = InvoiceTable
    table_pagination = {
        'per_page': 10
    }


class InvoiceItemInline(InlineFormSet):
    model = InvoiceItem
    form_class = InvoiceItemForm

    def get_factory_kwargs(self):
        kwargs = super(InvoiceItemInline, self).get_factory_kwargs()
        kwargs['extra'] = 0
        kwargs['min_num'] = 1
        return kwargs


class InvoiceNew(CosmicSaveErrorDialogsMixin, CreateWithInlinesView):
    template_name = 'demo_site/invoice_new.html'
    model = Invoice
    form_class = InvoiceForm
    inlines = [InvoiceItemInline]

    def get_context_data(self, **kwargs):
        data = super(InvoiceNew, self).get_context_data(**kwargs)
        data['formset_helper'] = CosmicFormsetHelper()
        return data


class InvoiceEdit(CosmicSaveErrorDialogsMixin, UpdateWithInlinesView):
    template_name = 'demo_site/invoice_edit.html'
    model = Invoice
    form_class = InvoiceForm
    inlines = [InvoiceItemInline]

    def get_context_data(self, **kwargs):
        data = super(InvoiceEdit, self).get_context_data(**kwargs)
        data['formset_helper'] = CosmicFormsetHelper()
        return data


class InvoiceDelete(DeleteView):
    template_name = 'demo_site/invoice_delete.html'
    model = Invoice

    def get_success_url(self):
        return reverse('invoices')
