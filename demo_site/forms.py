from django import forms
from django.urls import reverse
from django.forms.models import inlineformset_factory
from crispy_forms.layout import Div, Submit, HTML
from dal import autocomplete

from cosmicdb.forms import CosmicFormHelper
from demo_site.models import Customer, Invoice, InvoiceItem


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = (
            'code',
            'name',
            'phone',
        )

    def __init__(self, *args, **kwargs):
        super(CustomerForm, self).__init__(*args, **kwargs)
        self.helper = CosmicFormHelper(self)


class CustomerFormWithDelete(CustomerForm):
    def __init__(self, *args, **kwargs):
        super(CustomerFormWithDelete, self).__init__(*args, **kwargs)
        self.helper = CosmicFormHelper(self, submit_row=Div(Submit('save', 'Save', type='submit'), HTML('<a class="btn btn-danger" href="'+reverse('customer_delete', kwargs={'pk': self.instance.pk})+'">Delete</a>')))


class InvoiceForm(forms.ModelForm):
    total = forms.DecimalField(required=False)

    class Meta:
        model = Invoice
        fields = (
            'date',
            'customer',
        )

    def __init__(self, *args, **kwargs):
        super(InvoiceForm, self).__init__(*args, **kwargs)
        self.fields['total'].widget.attrs = {'value': self.instance.total(), 'readonly': 'readonly'}
        self.fields['customer'].widget = autocomplete.ModelSelect2(url='customer_autocomplete')
        self.fields['customer'].widget.choices = self.fields['customer'].choices
        self.helper = CosmicFormHelper(self, no_submit_row=True)
        self.helper.form_tag = False


class InvoiceItemForm(forms.ModelForm):
    total = forms.DecimalField(required=False)

    class Meta:
        model = InvoiceItem
        fields = (
            'description',
            'amount',
            'tax',
        )

    def __init__(self, *args, **kwargs):
        super(InvoiceItemForm, self).__init__(*args, **kwargs)
        self.fields['total'].widget.attrs = {'value': self.instance.total(), 'readonly': 'readonly'}
